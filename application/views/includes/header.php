<?php
$user_id = $this->session->userdata('user_id');
$user_em = $this->session->userdata('user_email');
$user_role = $this->session->userdata('user_role');
$user_outlet = $this->session->userdata('user_outlet');

if (empty($user_id)) {
    redirect(base_url(), 'refresh');
}

$tk_c = $this->router->class;
$tk_m = $this->router->method;


$alert_msg = $this->session->flashdata('alert_msg');
$settingResult = $this->db->get_where('site_setting');
$settingData = $settingResult->row();
$setting_site_name = $settingData->site_name;
$setting_pagination = $settingData->pagination;
$setting_tax = $settingData->tax;
$setting_currency = $settingData->currency;
$setting_date = $settingData->datetime_format;
$setting_product = $settingData->display_product;
$setting_keyboard = $settingData->display_keyboard;
$setting_customer_id = $settingData->default_customer_id;

if (isset($_COOKIE['outlet'])) {
    $this->load->helper('cookie');
    delete_cookie('outlet');
}
?>
<!DOCTYPE html>
<html ng-app="">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <title><?php echo $setting_site_name; ?></title>
        <!-- Favicon-->
        <link rel="icon" href="<?= base_url()?>favicon.ico" type="image/x-icon">

        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

        <!-- Bootstrap Core Css -->
        <link href="<?= base_url() ?>assets/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

        <!-- Custom Css -->
        <link href="<?= base_url() ?>assets/css/style.css" rel="stylesheet">

        <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
        <link href="<?= base_url() ?>assets/css/themes/all-themes.css" rel="stylesheet" />

        <script src="<?= base_url() ?>assets/plugins/jquery/jquery.min.js"></script>

    </head>

    <body class="theme-blue">
        <!-- Page Loader -->
        <div class="page-loader-wrapper">
            <div class="loader">
                <div class="preloader">
                    <div class="spinner-layer pl-red">
                        <div class="circle-clipper left">
                            <div class="circle"></div>
                        </div>
                        <div class="circle-clipper right">
                            <div class="circle"></div>
                        </div>
                    </div>
                </div>
                <p>Please wait...</p>
            </div>
        </div>
        <!-- #END# Page Loader -->
        <!-- Overlay For Sidebars -->
        <div class="overlay"></div>
        <nav class="navbar">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a href="javascript:void(0);" class="bars"></a>

                    <a class="navbar-brand" href="<?= base_url()?>dashboard"><?php echo $setting_site_name; ?></a>
                </div>
            </div>
        </nav>
        <!-- #Top Bar -->
        <section>
            <!-- Left Sidebar -->
            <aside id="leftsidebar" class="sidebar">
                <!-- User Info -->
                <div class="user-info">
                    <div class="image">
                        <img src="<?= base_url() ?>assets/images/user.png" width="48" height="48" alt="User" />
                    </div>
                    <div class="info-container">
                        <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo $this->session->userdata('fullname'); ?></div>
                        <div class="email"><?php echo $this->session->userdata('user_email'); ?></div>
                        <div class="btn-group user-helper-dropdown">
                            <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="javascript:void(0);"><i class="material-icons">person</i>Profile</a></li>
                                 <li role="seperator" class="divider"></li>
                                <li><a href="<?= base_url() ?>auth/logout"><i class="material-icons">input</i><?php echo $lang_logout; ?></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- #User Info -->
                <!-- Menu -->
                <div class="menu">
                    <ul class="list">
                        <li class="header">MAIN NAVIGATION</li>

                        <li <?php if ($tk_c == 'dashboard') { ?> class="active" <?php } ?>>
                            <a href="<?= base_url() ?>dashboard">
                                <i class="material-icons">home</i>
                                <span>Dashboard</span>
                            </a>
                        </li>

                        <li <?php if ($tk_c == 'customers') { ?> class="active" <?php } ?>>
                            <a href="<?= base_url() ?>customers/view">
                                <i class="material-icons">people</i>
                                <span><?php echo $lang_customers; ?></span>
                            </a>
                        </li>

                        <li <?php if ($tk_c == 'pos') { ?> class="active" <?php } ?>>
                            <a href="<?= base_url() ?>pos">
                                <i class="material-icons">add_shopping_cart</i>
                                <span><?php echo $lang_pos; ?></span>
                            </a>
                        </li>

                        <li <?php if ($tk_c == 'gift_card') { ?> class="active" <?php } ?>>
                            <a href="<?= base_url() ?>gift_card/list_gift_card">
                                <i class="material-icons">card_giftcard</i>
                                <span><?php echo $lang_gift_card; ?></span>
                            </a>
                        </li>

                        <li <?php if ($tk_m == 'list_sales') { ?> class="active" <?php } ?>>
                            <a href="<?= base_url() ?>sales/list_sales">
                            <i class="material-icons">attach_money</i>
                                <span><?php echo $lang_sales; ?></span>
                            </a>
                        </li>

                        <li <?php if (($tk_c == 'returnorder') || ($tk_c == 'pnl') || ($tk_c == 'reports')) { ?> class="active" <?php } ?>>
                            <a href="javascript:void(0);" class="menu-toggle">
                                <i class="material-icons">map</i>
                                <span>Reports</span>
                            </a>
                            <ul class="ml-menu">
                                <li <?php if ($tk_m == 'pnl_report') { ?> class="active" <?php } ?>>
                                    <a href="<?= base_url() ?>pnl/pnl_report">
                                        <span><?php echo $lang_pnl_report; ?></span>
                                    </a>
                                </li>

                                <li <?php if ($tk_m == 'pnl_graph_view') { ?> class="active" <?php } ?>>
                                    <a href="<?= base_url() ?>pnl/pnl_graph_view">
                                        <span><?php echo $lang_pnl; ?></span>
                                    </a>
                                </li>

                                <li <?php if ($tk_c == 'reports') { ?> class="active" <?php } ?>>
                                    <a href="<?= base_url() ?>reports/sales_report">
                                        <span><?php echo $lang_sales_report; ?></span>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <li <?php if (($tk_c == 'inventory') || ($tk_c == 'products') || ($tk_m == 'product_category') || ($tk_m == 'addproductcategory') || ($tk_m == 'editproductcategory')) { ?> class="active" <?php } ?>>
                            <a href="javascript:void(0);" class="menu-toggle">
                                <i class="material-icons">map</i>
                                <span>Products</span>
                            </a>
                            <ul class="ml-menu">

                                <li <?php if (($tk_c == 'products') && $tk_m == 'list_products') { ?> class="active" <?php } ?>>
                                    <a href="<?= base_url() ?>products/list_products">
                                        <span><?php echo $lang_products; ?></span>
                                    </a>
                                </li>

                                <li <?php if (($tk_c == 'products') && (($tk_m == 'product_category') || ($tk_m == 'addproductcategory') || ($tk_m == 'editproductcategory'))) { ?> class="active" <?php } ?>>
                                    <a href="<?= base_url() ?>products/product_category">
                                        <span><?php echo $lang_product_category; ?></span>
                                    </a>
                                </li>

                                <li <?php if (($tk_c == 'inventory') && ($tk_m == 'view')) { ?> class="active" <?php } ?>>
                                    <a href="<?= base_url() ?>inventory/view">
                                        <span><?php echo $lang_inventory; ?></span>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <li <?php if (($tk_c == 'outlets') || ($tk_c == 'addoutlet') || ($tk_m == 'editoutlet') || ($tk_m == 'users') || ($tk_m == 'adduser') || ($tk_m == 'edituser')) { ?> class="active" <?php } ?>>
                            <a href="javascript:void(0);" class="menu-toggle">
                                <i class="material-icons">map</i>
                                <span>Outlets And Employees Account</span>
                            </a>
                            <ul class="ml-menu">

                            <li <?php if (($tk_c == 'outlets') || ($tk_c == 'addoutlet') || ($tk_m == 'editoutlet')) { ?> class="active" <?php } ?>>
                                    <a href="<?= base_url() ?>setting/outlets">
                                        <span><?php echo $lang_outlets; ?></span>
                                    </a>
                                </li>

                                <li <?php if (($tk_m == 'users') || ($tk_m == 'adduser') || ($tk_m == 'edituser')) { ?> class="active" <?php } ?>>
                                    <a href="<?= base_url() ?>setting/users">
                                        <span><?php echo $lang_users; ?></span>
                                    </a>
                                </li>

                                <li <?php if (($tk_m == 'employee') || ($tk_m == 'addemployee') || ($tk_m == 'editemployee')) { ?> class="active" <?php } ?>>
                                    <a href="<?= base_url() ?>setting/employees">
                                        <span>Employee</span>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <li<?php if ($tk_c == 'expenses') { ?> class="active" <?php } ?>>
                            <a href="javascript:void(0);" class="menu-toggle">
                                <i class="material-icons">map</i>
                                <span>Expenses</span>
                            </a>
                            <ul class="ml-menu">
                            <li <?php if (($tk_c == 'expenses') && ($tk_m == 'view')) { ?> class="active" <?php } ?>>
                                    <a href="<?= base_url() ?>expenses/view">
                                        <span><?php echo $lang_expenses; ?></span>
                                    </a>
                                </li>

                                <li <?php if ($tk_m == 'expense_category') { ?> class="active" <?php } ?>>
                                    <a href="<?= base_url() ?>expenses/expense_category">
                                        <span><?php echo $lang_expenses_category; ?></span>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <li <?php if (($tk_m == 'suppliers') || ($tk_m == 'addsupplier') || ($tk_m == 'editsupplier') || ($tk_c == 'purchase_order')) { ?> class="active" <?php } ?>>
                            <a href="javascript:void(0);" class="menu-toggle">
                                <i class="material-icons">map</i>
                                <span>Suppliers</span>
                            </a>
                            <ul class="ml-menu">
                                <li <?php if (($tk_m == 'suppliers') || ($tk_m == 'addsupplier') || ($tk_m == 'editsupplier')) { ?> class="active" <?php } ?>>
                                    <a href="<?= base_url() ?>setting/suppliers">
                                        <span><?php echo $lang_suppliers; ?></span>
                                    </a>
                                </li>
                                <li <?php if ($tk_c == 'purchase_order') { ?> class="active" <?php } ?>>
                                    <a href="<?= base_url() ?>purchase_order/po_view">
                                        <span><?php echo $lang_purchase_order; ?></span>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <li <?php if ($tk_c == 'setting') { ?> class="active" <?php } ?>>
                            <a href="javascript:void(0);" class="menu-toggle">
                                <i class="material-icons">map</i>
                                <span>Settings</span>
                            </a>
                            <ul class="ml-menu">
                                <li <?php if (($tk_m == 'payment_methods') || ($tk_m == 'addpaymentmethod') || ($tk_m == 'editpaymentmethod')) { ?> class="active" <?php } ?>>
                                    <a href="<?= base_url() ?>setting/payment_methods">
                                        <span><?php echo $lang_payment_methods; ?></span>
                                    </a>
                                </li>

                                <li <?php if ($tk_m == 'system_setting') { ?> class="active" <?php } ?>>
                                    <a href="<?= base_url() ?>setting/system_setting">
                                        <span><?php echo $lang_system_setting; ?></span>
                                    </a>
                                </li>

                                
                            </ul>
                        </li>


                    </ul>
                </div>
                <!-- #Menu -->
                <!-- Footer -->
                <div class="legal">
                    <div class="copyright">
                        &copy; 2020 <a href="javascript:void(0);"><?php echo $setting_site_name; ?></a>.
                    </div>
                    <!--<div class="version">
                            <b>Version: </b> 1.0.0
                    </div>-->
                </div>
                <!-- #Footer -->
            </aside>
        </section>				