<?php
require_once 'includes/header.php';
?>
<link href="<?= base_url() ?>assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
<script src="<?= base_url() ?>assets/plugins/bootstrap-select/js/bootstrap-select.js"></script>
<section class="content">
    <div class="container-fluid">
        
		
		<ol class="breadcrumb breadcrumb-bg-cyan">
			<li><a href="<?= base_url() ?>dashboard"><i class="material-icons">home</i> Home</a></li>
			<li><a href="<?= base_url() ?>setting/employees"><i class="material-icons">person</i> Employee</a></li>
			<li><i class="material-icons">add</i> <?php echo $lang_add_new_employee; ?></li>
		</ol>

        <form action="<?= base_url() ?>setting/insertEmployee" method="post">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <?php
                            if (!empty($alert_msg)) {
                                $flash_status = $alert_msg[0];
                                $flash_header = $alert_msg[1];
                                $flash_desc = $alert_msg[2];
                                ?>
                                <?php if ($flash_status == 'failure') { ?>
                                    <div class="alert alert-info">
                                        <strong>Heads up!</strong> <?php echo $flash_desc; ?>
                                    </div>
                                <?php } ?>

                                <?php if ($flash_status == 'success') { ?>
                                    <div class="alert alert-success">
                                        <strong>Well done!</strong> <?php echo $flash_desc; ?>
                                    </div>
                                <?php } ?>
                            <?php } ?>

                            <h3 class="card-inside-title">Basic Infomation</h3>
                            <div class="row clearfix">

                                <div class="col-sm-6">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" name="fullname" class="form-control"  maxlength="499" autofocus autocomplete="off" value="<?php
                                            if (!empty($alert_msg)) {
                                                echo $alert_msg[3];
                                            }
                                            ?>" />
                                            <label class="form-label"><?php echo $lang_full_name; ?></label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="email" name="email" class="form-control" maxlength="254" autocomplete="off" value="<?php
                                            if (!empty($alert_msg)) {
                                                echo $alert_msg[4];
                                            }
                                            ?>" />
                                            <label class="form-label"><?php echo $lang_email; ?></label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" name="password" class="form-control" maxlength="499" autocomplete="off" value="<?php
                                            if (!empty($alert_msg)) {
                                                echo $alert_msg[5];
                                            }
                                            ?>" />
                                            <label class="form-label">Birthday</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" name="conpassword" class="form-control" maxlength="499" autocomplete="off" value="<?php
                                            if (!empty($alert_msg)) {
                                                echo $alert_msg[6];
                                            }
                                            ?>" />
                                            <label class="form-label">Gender</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" name="password" class="form-control" maxlength="499" autocomplete="off" value="<?php
                                            if (!empty($alert_msg)) {
                                                echo $alert_msg[5];
                                            }
                                            ?>" />
                                            <label class="form-label">Address</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" name="conpassword" class="form-control" maxlength="499" autocomplete="off" value="<?php
                                            if (!empty($alert_msg)) {
                                                echo $alert_msg[6];
                                            }
                                            ?>" />
                                            <label class="form-label">Contact No.</label>
                                        </div>
                                    </div>
                                </div>

                            </div>


                            <h5 class="card-inside-title">Incase of Emergency</h5>
                            <div class="row clearfix">

                                <div class="col-sm-6">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" name="fullname" class="form-control"  maxlength="499" autofocus autocomplete="off" value="<?php
                                            if (!empty($alert_msg)) {
                                                echo $alert_msg[3];
                                            }
                                            ?>" />
                                            <label class="form-label"><?php echo $lang_full_name; ?></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" name="fullname" class="form-control"  maxlength="499" autofocus autocomplete="off" value="<?php
                                            if (!empty($alert_msg)) {
                                                echo $alert_msg[3];
                                            }
                                            ?>" />
                                            <label class="form-label">Relation with the Person</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" name="fullname" class="form-control"  maxlength="499" autofocus autocomplete="off" value="<?php
                                            if (!empty($alert_msg)) {
                                                echo $alert_msg[3];
                                            }
                                            ?>" />
                                            <label class="form-label">Contact No.</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" name="fullname" class="form-control"  maxlength="499" autofocus autocomplete="off" value="<?php
                                            if (!empty($alert_msg)) {
                                                echo $alert_msg[3];
                                            }
                                            ?>" />
                                            <label class="form-label">Address</label>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <h5 class="card-inside-title">Benefits</h5>
                            <div class="row clearfix">

                                <div class="col-sm-6">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" name="fullname" class="form-control"  maxlength="499" autofocus autocomplete="off" value="<?php
                                            if (!empty($alert_msg)) {
                                                echo $alert_msg[3];
                                            }
                                            ?>" />
                                            <label class="form-label">SSS Number</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" name="fullname" class="form-control"  maxlength="499" autofocus autocomplete="off" value="<?php
                                            if (!empty($alert_msg)) {
                                                echo $alert_msg[3];
                                            }
                                            ?>" />
                                            <label class="form-label">Philhealth Number</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" name="fullname" class="form-control"  maxlength="499" autofocus autocomplete="off" value="<?php
                                            if (!empty($alert_msg)) {
                                                echo $alert_msg[3];
                                            }
                                            ?>" />
                                            <label class="form-label">PAGIBIG Number</label>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <h5 class="card-inside-title">Role</h5>
                            <div class="row clearfix">

                                <div class="col-sm-6">
                                        <!--<p><?php echo $lang_role; ?></p>-->
                                    <select name="role" class="form-control show-tick" required>
                                        <option value=""><?php echo $lang_choose_role; ?></option>
                                        <?php
                                        $roleData = $this->Constant_model->getDataAll('user_roles', 'id', 'ASC','created_user_id',$user_id);
                                        for ($r = 0; $r < count($roleData); ++$r) {
                                            $role_id = $roleData[$r]->id;
                                            $role_name = $roleData[$r]->name;

                                            if ($user_role == 2) {
                                                if ($role_id == 1) {
                                                    continue;
                                                }
                                            }
                                            if ($user_role == 3) {
                                                if ($role_id < 3) {
                                                    continue;
                                                }
                                            }
                                            ?>
                                            <option value="<?php echo $role_id; ?>" <?php
                                            if (!empty($alert_msg)) {
                                                if ($alert_msg[7] == $role_id) {
                                                    echo 'selected="selected"';
                                                }
                                            }
                                            ?>>
                                            <?php echo $role_name; ?>
                                            </option>
                                            <?php
                                            }
                                            ?>
                                    </select>
                                </div>


                                <div class="col-sm-6">
                                        <!--<p><?php echo $lang_outlets; ?></p>-->
                                    <select name="outlet" class="form-control show-tick" >
                                        <option value=""><?php echo $lang_choose_outlet; ?></option>
                                        <?php
                                        if ($user_role == 1) {
                                            $outletData = $this->Constant_model->getDataOneColumnSortColumn('outlets', 'created_user_id', $user_id, 'name', 'ASC');
                                        } else {
                                            $outletData = $this->Constant_model->getDataOneColumn('outlets', 'id', "$user_outlet");
                                        }
                                        for ($u = 0; $u < count($outletData); ++$u) {
                                            $outlet_id = $outletData[$u]->id;
                                            $outlet_name = $outletData[$u]->name;
                                            ?>
                                            <option value="<?php echo $outlet_id; ?>" <?php
                                            if (!empty($alert_msg)) {
                                                if ($alert_msg[8] == $outlet_id) {
                                                    echo 'selected="selected"';
                                                }
                                            }
                                            ?>>
                                            <?php echo $outlet_name; ?>
                                            </option>
                                            <?php
                                            }
                                            ?>
                                    </select>
                                </div>
                            </div>


                            <div class="row clearfix">

                                <div class="col-sm-4">
                                    <div class="form-group form-float">
                                        <div class="form-line">

                                        <label>Resume <span>*</span></label>
                                        <br />
                                        <input id="uploadFile" readonly/>
                                        <div class="fileUpload btn btn-primary">
                                            <span>Browse</span>
                                            <input id="uploadBtn" name="uploadFile" type="file" class="upload" />
                                        </div>

                                    </div>
                                </div>
                            </div>


                            </div>


                            <div class="row clearfix">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <button class="btn btn-primary"><?php echo $lang_add; ?></button>
                                        <a class="btn btn-primary" href="<?= base_url() ?>setting/employees"><?php echo $lang_back; ?></a>
                                    </div>


                                </div>
                            </div>
                        </div>

                    </div><!-- Panel Body // END -->
                </div><!-- Panel Default // END -->
            </div><!-- Col md 12 // END -->
    </div><!-- Row // END -->
</form>


</div>
</div>

<?php
require_once 'includes/footer.php';
?>			