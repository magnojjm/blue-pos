<?php
require_once 'includes/header.php';

$siteDtaData = $this->Constant_model->getDataOneColumn('site_setting', 'site_setting_for_user', $user_id);

$site_name = $siteDtaData[0]->site_name;
$timezone = $siteDtaData[0]->timezone;
$pagination = $siteDtaData[0]->pagination;
$tax = $siteDtaData[0]->tax;
$currency = $siteDtaData[0]->currency;
$dtm_format = $siteDtaData[0]->datetime_format;
$display_product = $siteDtaData[0]->display_product;
$keyborad = $siteDtaData[0]->display_keyboard;
$def_cust_id = $siteDtaData[0]->default_customer_id;
$site_logo = $siteDtaData[0]->site_logo;
?>
<link href="<?= base_url() ?>assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
<script src="<?= base_url() ?>assets/plugins/bootstrap-select/js/bootstrap-select.js"></script>
<link href="<?= base_url() ?>assets/plugins/dropzone/dropzone.css" rel="stylesheet">
<script src="<?= base_url() ?>assets/plugins/dropzone/dropzone.js"></script>

<section class="content">
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                <ol class="breadcrumb breadcrumb-bg-cyan">
                    <li><a href="<?= base_url() ?>"><i class="material-icons">home</i> Home</a></li>
                    <li class="active"><i class="material-icons">settings</i> <?php echo $lang_system_setting; ?></li>
                </ol>

                <form action="<?= base_url() ?>setting/updateSiteSetting" method="post" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <?php
                                    if (!empty($alert_msg)) {
                                        $flash_status = $alert_msg[0];
                                        $flash_header = $alert_msg[1];
                                        $flash_desc = $alert_msg[2];
                                        ?>
                                        <?php if ($flash_status == 'failure') { ?>
                                            <div class="alert alert-info">
                                                <strong>Heads up!</strong> <?php echo $flash_desc; ?>
                                            </div>
                                        <?php } ?>


                                        <?php if ($flash_status == 'success') { ?>
                                            <div class="alert alert-success">
                                                <strong>Well done!</strong> <?php echo $flash_desc; ?>
                                            </div>
                                        <?php } ?>
                                    <?php } ?>

                                    <h3 class="card-inside-title">Infomation</h3>
                                    <div class="row clearfix">
                                        <div class="col-sm-6">
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <input type="text" name="site_name" class="form-control" maxlength="499" autofocus required value="<?php echo $site_name; ?>" autocomplete="off" />
                                                    <label class="form-label"><?php echo $lang_site_name; ?></label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <input type="text" name="tax" class="form-control" maxlength="499" autofocus required value="<?php echo $tax; ?>" autocomplete="off" />
                                                    <label class="form-label"><?php echo $lang_tax; ?></label>
                                                </div>
                                            </div>
                                        </div>


                                    </div>

                                    <h3 class="card-inside-title">Admin Settings</h3>
                                    <div class="row clearfix">
                                        <div class="col-sm-6">
                                            <strong><p><?php echo $lang_system_date_format; ?></p></strong>
                                            <select  name="date_format" class="form-control show-tick">
                                                <option value="Y-m-d" <?php
                                                if ($dtm_format == 'Y-m-d') {
                                                    echo 'selected="selected"';
                                                }
                                                ?>>yy-mm-dd</option>
                                                <option value="Y.m.d" <?php
                                                if ($dtm_format == 'Y.m.d') {
                                                    echo 'selected="selected"';
                                                }
                                                ?>>yy.mm.dd</option>
                                                <option value="Y/m/d" <?php
                                                if ($dtm_format == 'Y/m/d') {
                                                    echo 'selected="selected"';
                                                }
                                                ?>>yy/mm/dd</option>
                                                <option value="m-d-Y" <?php
                                                if ($dtm_format == 'm-d-Y') {
                                                    echo 'selected="selected"';
                                                }
                                                ?>>mm-dd-yy</option>
                                                <option value="m.d.Y" <?php
                                                if ($dtm_format == 'm.d.Y') {
                                                    echo 'selected="selected"';
                                                }
                                                ?>>mm.dd.yy</option>
                                                <option value="m/d/Y" <?php
                                                if ($dtm_format == 'm/d/Y') {
                                                    echo 'selected="selected"';
                                                }
                                                ?>>mm/dd/yy</option>
                                                <option value="d-m-Y" <?php
                                                if ($dtm_format == 'd-m-Y') {
                                                    echo 'selected="selected"';
                                                }
                                                ?>>dd-mm-yy</option>
                                                <option value="d.m.Y" <?php
                                                if ($dtm_format == 'd.m.Y') {
                                                    echo 'selected="selected"';
                                                }
                                                ?>>dd.mm.yy</option>
                                                <option value="d/m/Y" <?php
                                                if ($dtm_format == 'd/m/Y') {
                                                    echo 'selected="selected"';
                                                }
                                                ?>>dd/mm/yy</option>
                                            </select>
                                        </div>



                                        <div class="col-sm-6">
                                            <strong><p><?php echo $lang_currency; ?></p></strong>
                                            <select name="currency" class="form-control show-tick" >
                                                <?php
                                                $currencyData = $this->Constant_model->getDataAll2('currency', 'iso', 'ASC');
                                                for ($c = 0; $c < count($currencyData); ++$c) {
                                                    $currency_iso = $currencyData[$c]->iso;
                                                    ?>
                                                    <option value="<?php echo $currency_iso; ?>" <?php
                                                    if ($currency_iso == $currency) {
                                                        echo 'selected="selected"';
                                                    }
                                                    ?>>
                                                                <?php echo $currency_iso; ?>
                                                    </option>
                                                    <?php
                                                    unset($currency_iso);
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <h3 class="card-inside-title">Admin Settings</h3>
                                    <div class="row clearfix">
                                        <div class="col-sm-6">
                                            <strong><p><?php echo $lang_pagination_per_page; ?></p></strong>
                                            <select name="pagination" class="form-control show-tick">
                                                <option value="10" <?php
                                                if ($pagination == '10') {
                                                    echo 'selected="selected"';
                                                }
                                                ?>>10</option>
                                                <option value="20" <?php
                                                if ($pagination == '20') {
                                                    echo 'selected="selected"';
                                                }
                                                ?>>20</option>
                                                <option value="50" <?php
                                                        if ($pagination == '50') {
                                                            echo 'selected="selected"';
                                                        }
                                                        ?>>50</option>
                                                <option value="100" <?php
                                                        if ($pagination == '100') {
                                                            echo 'selected="selected"';
                                                        }
                                                        ?>>100</option>
                                            </select>
                                        </div>

                                        <div class="col-sm-6">
                                            <strong><p><?php echo $lang_pos_display_product; ?></p></strong>
                                            <select name="display_product" class="form-control show-tick">
                                                <option value="1" <?php
                                                        if ($display_product == '1') {
                                                            echo 'selected="selected"';
                                                        }
                                                        ?>><?php echo $lang_name; ?></option>
                                                <option value="2" <?php
                                                        if ($display_product == '2') {
                                                            echo 'selected="selected"';
                                                        }
                                                        ?>><?php echo $lang_photo; ?></option>
                                                <option value="3" <?php
                                                if ($display_product == '3') {
                                                    echo 'selected="selected"';
                                                }
                                                ?>><?php echo $lang_both; ?></option>
                                            </select>
                                        </div>

                                        <div class="col-sm-6">
                                            <strong><p><?php echo $lang_system_timezone; ?></p></strong>
                                            <select name="timezone" class="form-control show-tick">
                                                    <?php
                                                    $timeZoneData = $this->Constant_model->getDataAll2('timezones', 'timezone', 'ASC');
                                                    for ($t = 0; $t < count($timeZoneData); ++$t) {
                                                        $timezone_name = $timeZoneData[$t]->timezone;
                                                        ?>
                                                    <option value="<?php echo $timezone_name; ?>" <?php
                                                            if ($timezone_name == $timezone) {
                                                                echo 'selected="selected"';
                                                            }
                                                            ?>><?php echo $timezone_name; ?></option>
                                                        <?php unset($timezone_name);
                                                    }
                                                    ?>
                                            </select>
                                        </div>

                                        <!-- <div class="col-sm-6">
                                            <strong><p><?php echo $lang_pos_display_keyboard; ?></p></strong>
                                            <select name="display_keyboard" class="form-control show-tick">
                                                <option value="1" <?php
                                                if ($keyborad == '1') {
                                                    echo 'selected="selected"';
                                                }
                                                ?>><?php echo $lang_yes; ?></option>
                                                <option value="0" <?php
                                                        if ($keyborad == '0') {
                                                            echo 'selected="selected"';
                                                        }
                                                ?>><?php echo $lang_no; ?></option>
                                            </select>
                                        </div> -->

                                        <div class="col-sm-6">
                                            <strong><p><?php echo $lang_pos_default_customer; ?></p></strong>
                                            <select name="display_keyboard" class="form-control show-tick">
                                                <?php
                                                $customerData = $this->Constant_model->getDataAll('customers', 'id', 'ASC','created_user_id',$user_id);
                                                for ($u = 0; $u < count($customerData); ++$u) {
                                                    $customer_id = $customerData[$u]->id;
                                                    $customer_name = $customerData[$u]->fullname;
                                                    ?>
                                                    <option value="<?php echo $customer_id; ?>" <?php
                                                    if ($customer_id == $def_cust_id) {
                                                        echo 'selected="selected"';
                                                    }
                                                    ?>><?php echo $customer_name; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="col-sm-6">
                                            <label><?php echo $lang_logo; ?></label>
                                            <input id="uploadFile" readonly />
                                            <div class="fileUpload btn btn-primary">
                                                <span><?php echo $lang_browse; ?></span>
                                                <input id="uploadBtn" name="uploadFile" type="file" class="upload" />
                                            </div>
                                            <img src="<?= base_url() ?>assets/img/logo/<?php echo $site_logo; ?>" height="100px" />
                                        </div>
                                    </div>
                                        <div class="row clearfix">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <button class="btn btn-primary"><?php echo $lang_update_system_setting; ?></button>
                                                    </div>
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div>
                </form>
            </div>
        </div>
    </div>

</section>



<?php
require_once 'includes/footer.php';
?>			