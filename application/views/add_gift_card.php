<?php require_once 'includes/header.php'; ?>

<link rel="stylesheet" href="<?= base_url() ?>assets/js/jquery-ui.css">
<script src="<?= base_url() ?>assets/js/jquery-1.12.4.js"></script>
<script src="<?= base_url() ?>assets/js/jquery-ui.js"></script>


<script src="<?= base_url() ?>assets/js/input-mask/jquery.inputmask.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/js/input-mask/jquery.inputmask.date.extensions.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/js/custom.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#btn-input').inputmask("9999999");
        $('#btn-todo').click(function () {
            var numb = generateCardNo();
            document.getElementById("btn-input").value = numb;

            return false;
        });
    });
</script> 

<section class="content">
    <div class="container-fluid">
    <ol class="breadcrumb breadcrumb-bg-cyan">
        <li><a href="<?php echo base_url() ?>"><i class="material-icons">home</i> Home</a></li>
        <li><a href="<?php echo base_url() ?>/gift_card/list_gift_card"><i class="material-icons">card_giftcard</i> <?php echo $lang_gift_card; ?></a></li>
        <li class="active"><i class="material-icons">mode_edit</i> <?php echo $lang_add_gift_card; ?></li>
    </ol>

    <form action="<?= base_url() ?>gift_card/insertGiftCard" method="post">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">

                        <?php
                        if (!empty($alert_msg)) {
                            $flash_status = $alert_msg[0];
                            $flash_header = $alert_msg[1];
                            $flash_desc = $alert_msg[2];

                            if ($flash_status == 'failure') {
                                ?>
                                <div class="row" id="notificationWrp">
                                    <div class="col-md-12">
                                        <div class="alert bg-warning" role="alert">
                                            <i class="icono-exclamationCircle" style="color: #FFF;"></i> 
                                            <?php echo $flash_desc; ?> <i class="icono-cross" id="closeAlert"></i>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                            if ($flash_status == 'success') {
                                ?>
                                <div class="row" id="notificationWrp">
                                    <div class="col-md-12">
                                        <div class="alert bg-success" role="alert">
                                            <i class="icono-check"></i> 
                                            <?php echo $flash_desc; ?> <i class="icono-cross" id="closeAlert"></i>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                        }
                        ?>

                    <h3 class="card-inside-title">Add Gift Card</h3>
                    <div class="row clearfix">

                        <div class="col-md-6">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <label><?php echo $lang_gift_card_number; ?> <span>*</span></label>

                                    <div class="input-group">
                                        <input id="btn-input" type="text" class="form-control" name="gift_card_numb" required autocomplete="off" />
                                        <span class="input-group-btn">
                                            <button class="btn btn-primary btn-md" id="btn-todo">
                                                <i class="icono-gear"></i>
                                                <?php echo $lang_generate; ?>
                                            </button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-sm-6">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <label><?php echo $lang_value; ?> (<?php echo $site_currency; ?>)</label>
                                    <input type="text" name="value" class="form-control" maxlength="10" autofocus autocomplete="off" required />
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <label><?php echo $lang_expiry_date; ?> </label>
                                    <input type="text" name="expiry_date" class="form-control" id="startDate" autocomplete="off" required />
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="row clearfix">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <button class="btn btn-primary"><?php echo $lang_add; ?></button>
                                        <a class="btn btn-primary" href="<?= base_url() ?>gift_card/list_gift_card"><?php echo $lang_back; ?></a>
                                    </div>


                                </div>
                            </div>


                    </div>

                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

</section>

<script>
    $(function () {
        $("#startDate").datepicker({
            dateFormat: "<?php echo $dateformat; ?>",
            autoclose: true
        });
    });
</script>


<?php require_once 'includes/footer.php'; ?>