<?php

//defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_model extends CI_Model {

    public function __construct() {
        // Call the Model constructor
        parent::__construct();
        $this->load->database();
    }

    public function verifyLogIn($data) {
        $email = $data['email'];
        $password = $this->encryptPassword($data['password']);

        $this->db->where('email',$email);
        $this->db->where('password',$password);
        $query = $this->db->get('users');

        if ($query->num_rows() == 1) {

            $user_data = $query->row_array();
            $result = array();
                if ($user_data['status'] == 0) {
                    $result['valid'] = false;
                    $result['error'] = 'Your account is suspended! Please contact to Administrator!';
                    $message = 'Your account is suspended! Please contact to Administrator!';
                    echo "<script type='text/javascript'>alert('$message');</script>";

                } else {
                    $result['valid'] = true;
                    $result['user_id'] = $user_data['id'];
                    $result['user_email'] = $user_data['email'];
                    $result['role_id'] = $user_data['role_id'];
                    $result['outlet_id'] = $user_data['outlet_id'];
                    $result['account_type'] = $user_data['account_type'];
                }
        
            return $result;
        } else {
            $result['valid'] = false;
            $result['error'] = 'Email Address do not exist at the system!';
            $message = 'Email Address do not exist at the system!';
            echo "<script type='text/javascript'>alert('$message');</script>";
            return $result;
        }
    }

    public function encryptPassword($password) {
        return md5("$password");
    }

}
